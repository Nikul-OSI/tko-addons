from odoo import http
from odoo.http import request


class OdooMigrationExpertsSourceControllers(http.Controller):

    @staticmethod
    def _json_response(data=[], error=False):
        response = {'status': '200 OK',
                    'status_code': 200,
                    'data': data,
                    'error': error}
        return response

    @staticmethod
    def _check_required_params(values, params):
        error = False
        for item in params:
            if item not in values:
                error += '%s\n' % item
        if error:
            error = 'Missing required parameters:\n' + error
        if not error:
            authorized_token = request.env['ir.config_parameter'].sudo().get_param('migration_experts_token')
            access_token = values['token']
            if not access_token or not authorized_token or authorized_token != access_token:
                error = 'Access Error'
        return error

    @staticmethod
    def _get_tables():
        query = "SELECT tablename FROM pg_tables WHERE schemaname = 'public'"
        request.env.cr.execute(query)
        res = request.env.cr.fetchall()
        return [r[0] for r in res]

    def _table_exist(self, table):
        return True if table in self._get_tables() else False

    @staticmethod
    def _where(table_name, latest_create_date, latest_write_date):
        where = ''
        if latest_create_date or latest_write_date:
            query = "SELECT column_name " \
                    "FROM information_schema.columns " \
                    "WHERE table_name = '%s' and " \
                    "(column_name='create_date' or column_name='write_date')" % table_name
            request.env.cr.execute(query)
            res = request.env.cr.fetchall()
            if res:
                where = 'WHERE'
                if latest_create_date and ('create_date',) in res:
                    where = "%s create_date IS NULL OR create_date > '%s' OR" % (where, latest_create_date)
                if latest_write_date and ('write_date',) in res:
                    where = "%s write_date IS NULL OR write_date > '%s' OR" % (where, latest_write_date)
                where = where[:-3]
        return where

    @http.route('/ome/test', type='json', auth='public', csrf=False, website=True, sitemap=False)
    def test_connection(self):
        data = []
        req = dict(request.jsonrequest)
        error = self._check_required_params(req, ['token'])
        if not error:
            try:
                request.env.cr.execute("SELECT 'ok'")
                data = request.env.cr.fetchall()[0][0]
            except Exception as e:
                error = e
        return self._json_response(data=data, error=error)

    ## To test in terminal:
    ## curl -i --data '{"token":"AAAAA", "table":"res_partner"}' http://database.domain/ome/get/data --header 'Content-Type: application/json' --header 'Accept: application/json'
    @http.route('/ome/read', type='json', auth='public', csrf=False, website=True, sitemap=False)
    def read(self):
        data = []
        req = dict(request.jsonrequest)
        error = self._check_required_params(req, ['token', 'table'])
        if not error:
            table = req.get('table')
            if self._table_exist(table):
                limit = req.get('limit', False)
                offset = req.get('offset', False)
                latest_create_date = req.get('latest_create_date', False)
                latest_write_date = req.get('latest_write_date', False)
                order = ''
                if req.get('order', False):
                    order_elem = [o.strip().split(' ') for o in req.get('order', False).split(',')]
                    for o in order_elem:
                        if len(o) == 1:
                            order = '%s, "%s"' % (order, o[0])
                        else:
                            order = '%s, "%s" %s' % (order, o[0], o[1].upper())
                    order = order[2:]
                query = 'SELECT JSON_AGG(t) FROM (SELECT * FROM "%s" %s %s %s %s) t' % (
                    table,
                    self._where(table, latest_create_date, latest_write_date),
                    'ORDER BY %s' % order if order else '',
                    "LIMIT '%s'" % limit if limit else '',
                    "OFFSET '%s'" % offset if offset else '')
                try:
                    request.env.cr.execute(query)
                    data = request.env.cr.fetchall()[0][0]
                except Exception as e:
                    error = e
            else:
                error = 'ERROR: Table %s not found in %s' % (table, request.env.cr.dbname)
        return self._json_response(data=data, error=error)

    @http.route('/ome/count', type='json', auth='public', csrf=False, website=True, sitemap=False)
    def count(self):
        data = []
        req = dict(request.jsonrequest)
        error = self._check_required_params(req, ['token', 'table'])
        if not error:
            table = req.get('table')
            if self._table_exist(table):
                latest_create_date = req.get('latest_create_date', False)
                latest_write_date = req.get('latest_write_date', False)
                query = 'SELECT COUNT(*) FROM "%s" %s' % (
                    table,
                    self._where(table, latest_create_date, latest_write_date))
                try:
                    request.env.cr.execute(query)
                    data = request.env.cr.fetchall()[0][0]
                except Exception as e:
                    error = e
            else:
                error = 'ERROR: Table %s not found in %s' % (table, request.env.cr.dbname)
        return self._json_response(data=data, error=error)

    @http.route('/ome/ids', type='json', auth='public', csrf=False, website=True, sitemap=False)
    def current_ids(self):
        data = []
        req = dict(request.jsonrequest)
        error = self._check_required_params(req, ['token', 'table'])
        if not error:
            table = req.get('table')
            if self._table_exist(table):
                query = 'SELECT id FROM "%s" ORDER BY id' % table
                try:
                    request.env.cr.execute(query)
                    data = [d[0] for d in request.env.cr.fetchall()]
                except Exception as e:
                    error = e
            else:
                error = 'ERROR: Table %s not found in %s' % (table, request.env.cr.dbname)
        return self._json_response(data=data, error=error)

    @http.route('/ome/tables', type='json', auth='public', csrf=False, website=True, sitemap=False)
    def tables(self):
        data = []
        req = dict(request.jsonrequest)
        error = self._check_required_params(req, ['token'])
        if not error:
            data = self._get_tables()
        return self._json_response(data=data, error=error)
