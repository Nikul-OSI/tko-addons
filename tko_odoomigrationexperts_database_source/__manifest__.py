# -*- coding: utf-8 -*-
{
    'name': 'Odoo Migration Experts Source Database',
    'description': '''Migrate any Odoo version to any version from Odoo Migration Experts.
This module is to be installed in source database. It's a read only access for enhanced security.''',
    'category': 'Technical',
    'version': '11.0.0.1',
    'sequence': 2,
    'author': 'Odoo Migration Experts',
    'license': 'OPL-1',
    'website': 'https://odoomigrationexperts.com',
    'support': 'info@odoomigrationexperts.com',
    'depends': [
        'base_setup',
        'mail',
    ],
    'data': [
        'data/ir.config.parameter.xml',
        'views/res_config.xml',
    ],
    'application': True,
    'installable': True,
    'auto_install': False,
}
