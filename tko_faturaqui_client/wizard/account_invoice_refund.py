from odoo import api, fields, models, _
from odoo.exceptions import Warning

class AccountInvoiceRefund(models.TransientModel):
    _inherit = 'account.invoice.refund'

    # Removed 'refund' option
    filter_refund = fields.Selection([
        ('cancel', 'Cancel: create credit note and reconcile'),
        ('modify', 'Modify: create credit note, reconcile and create a new draft invoice')
    ], default='cancel', string='Refund Method', required=True, help='Refund base on this type. You can not Modify and Cancel if the invoice is already reconciled')

    @api.multi
    def invoice_refund(self):
        inv_obj = self.env['account.invoice']
        context = dict(self._context or {})

        for form in self:
            for inv in inv_obj.browse(context.get('active_ids')):
                if inv.residual != inv.amount_total:
                    raise Warning("You can not create credit note for partially paid invoices.")
        return super(AccountInvoiceRefund, self).invoice_refund()


    @api.model
    def default_get(self, fields):
        res = super(AccountInvoiceRefund, self).default_get(fields)
        active_ids = self._context.get('active_ids')
        for inv in self.env['account.invoice'].browse(active_ids):
            res['description'] = 'Valores Inválidos #%s' %inv.number
        return res